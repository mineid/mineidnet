#!/bin/bash

echo '* Installing oauth2 Ruby gem'
gem install oauth2
echo '* Done installing oauth2 gem'
echo ''

echo '* Enter your API ID:'
read apiid

echo '* Enter your API secret:'
read apisecret

echo '* Enter the OAuth2 site: (e.g. https://mysite.com):'
read site

authorize_url=`ruby -r oauth2 -e "puts OAuth2::Client.new(\"$apiid\", \"$apisecret\", :site=>\"$site\").auth_code.authorize_url(:redirect_uri => 'http://localhost/oauth2/callback')"`

echo ''
echo "* You are about to be redirected to $authorize_url"
echo ''

echo "***After authorizing, you'll be brought to a non-existent page.  Copy the token in the URL you're redirected to (after 'code=') and paste below when prompted.***"
echo ''
echo '* Press ENTER to proceed!'
read
open $authorize_url

echo '* Enter your authorization code from the previous step:'
read auth_code
token=`ruby -r oauth2 -e "puts OAuth2::Client.new(\"$apiid\", \"$apisecret\", :site=>\"$site\").auth_code.get_token('$auth_code', :redirect_uri => 'http://localhost/oauth2/callback').token"`

echo ''
echo 'Your OAuth2 token is: '
echo $token
