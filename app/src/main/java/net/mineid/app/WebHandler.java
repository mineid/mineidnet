package net.mineid.app;

import static spark.Spark.*;
import org.apache.logging.log4j.*;
import org.json.*;
import java.util.*;

public class WebHandler {

    private static final Logger log = LogManager.getLogger();

    private final MinecraftService ms;

    public WebHandler(MinecraftService ms) {
        this.ms = ms;
        //staticFiles.location("/public");

        before("/*", (req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "origin, content-type, accept");
            res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
        });

        post("/api/v1/internal/authenticate", (req, res) -> {
            if(!req.queryParams().contains("type")) {
                return error(400, "Missing type field");
            }
            if(!req.queryParams().contains("username")) {
                return error(400, "Missing username field");
            }
            if(!req.queryParams().contains("password")) {
                return error(400, "Missing password field");
            }
            if(!req.queryParams().contains("remember")) {
                return error(400, "Missing remember field");
            }
            String username = ms.checkLogin(req.queryParams("username"), req.queryParams("password"));
            if(username == null) {
                return error(401, "Invalid credentials");
            }
	    UUID uuid = ms.getUUIDbyUsername(username);
            return message(200, Map.of("username", username, "uuid", uuid.toString())); // TODO return some kind of token as well
        });
    }

    private String error(int code, String message) {
        return message(code, Map.of("message", message));
    }

    private String message(int code, Map<String, String> data) {
        JSONObject o = new JSONObject();
        o.put("status", code);
        for(Map.Entry<String, String> e : data.entrySet()) {
            o.put(e.getKey(), e.getValue());
        }
        return o.toString();
    }

}
