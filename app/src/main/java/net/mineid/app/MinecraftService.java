package net.mineid.app;

import org.apache.logging.log4j.*;
import net.cydhra.nidhogg.*;
import net.cydhra.nidhogg.data.*;
import net.cydhra.nidhogg.exception.*;
import lombok.*;
import java.util.*;
import java.time.*;

public class MinecraftService {

    private static final Logger log = LogManager.getLogger();

    public MinecraftService() {
    }

    public String checkLogin(@NonNull String username, @NonNull String password) {
        try {
            return new YggdrasilClient().login(new AccountCredentials(username, password), YggdrasilAgent.MINECRAFT).getAlias();
        } catch (Exception e) {
		log.catching(e);
            return null;
        }
    }

    public boolean checkCode(int code) {
        return false;
    }

    public UUID getUUIDbyUsername(String username){
        try {
            return new MojangClient().getUUIDbyUsername(username, Instant.now()).get();
        } catch (Exception e) {
            return null;
        }

    }

}
