package net.mineid.app;

import static spark.Spark.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import org.apache.logging.log4j.*;
import java.io.*;
import java.util.*;

public class MineID {

    private final Logger log = LogManager.getLogger();

    private final MinecraftService ms;
    private final WebHandler web;

    public static void main(String[] args) {
        new MineID();
    }

    public MineID() {
        // Debug
        log.trace("TRACE");
        log.debug("DEBUG");
        log.error("ERROR");
        log.fatal("FATAL");
        log.info("INFO");
        log.warn("WARN");

        // Init ESAPI
        log.info("= Opening ESAPI =");
        EventStorageAPI.registerInit(new StorageYAML(new EventStoragePlugin() {
            public List<String> getAuthors() {
                return null;
            }
            public String getVersion() {
                return null;
            }
            public void callEvent(ESAPIEvent event, Object... data) {
            }
        }, new File("data"), (String s) -> getClass().getResourceAsStream(s)));
        EventStorageAPI.getStorage().open();

        // Init ESAPI
        log.info("= Init MinecraftService =");
        ms = new MinecraftService();

        // Init ESAPI
        log.info("= Init WebHandler HTTP interface =");
        web = new WebHandler(ms);
    }

}
