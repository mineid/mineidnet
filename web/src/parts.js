import React from 'react';
import * as utils from './utils.js';

export class Button extends React.Component {
  render() {
    return (
        <button className={"btn border-bottom-title btn-" + (this.props.type || "primary") + " " + (this.props.className || "")}>{this.props.children}</button>
    );
  }
}

export class LinkButton extends React.Component {
  render() {
    return (
      <a href={this.props.href} className="link-no-style">
      <Button type={this.props.type} className={(this.props.className || "")}>
        {this.props.children}
      </Button>
      </a>
    );
  }
}

export class SignInButton extends React.Component {
  render() {
    if(!utils.getStorage().getItem("username")){
      return (
        <LinkButton type="warning" href="/login" className={"my-2 my-md-0 text-white font-weight-bold " + (this.props.className || "")}>Sign In</LinkButton>
      );
    } else {
      return (
        <LinkButton type="warning" href="/logout" className={"my-2 my-md-0 text-white font-weight-bold " + (this.props.className || "")}>Logout</LinkButton>
      );
    }
  }
}

export class Anchor extends React.Component {
  render() {
    return (
        <div className="anchor" id={this.props.id}></div>
    );
  }
}

export class Title extends React.Component {
  render() {
    return (
    <h1 className={"d-inline font-weight-bold border-bottom-title border-" + ( this.props.type || "warning")}>{this.props.children}</h1>
    );
  }
}

export class Subtitle extends React.Component {
  render() {
    return (
    <h3 className={"d-inline font-weight-bold border-bottom-title border-" + ( this.props.type || "warning") + " " + ( this.props.className || "")}>{this.props.children}</h3>
    );
  }
}
