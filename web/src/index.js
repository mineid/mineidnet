import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './style.css';
import * as utils from './utils.js';
import {Button, LinkButton, Title, Subtitle, Anchor, SignInButton} from './parts.js';

class MainIntro extends React.Component {
  render() {
    return (
<div className="jumbotron my-0">
  <center>
    <img src="/assets/img/MineID.png" className="img-fluid mb-4" alt="MineID logo" />
    <h1 className=""><span className="font-weight-bold">"Login with Minecraft"</span><br/>OAuth2 Provider</h1>
    <hr className="my-4" />
    <p className="py-3">Stop typing your Minecraft password into insecure websites. Start using <b>MineID.net</b> today!</p>
    <p className="lead">
      <LinkButton type="warning" href="#how-it-works" className="btn-lg text-white">Tell me more!</LinkButton>
    </p>
  </center>
</div>
    );
  }
}

class About extends React.Component {
  render() {
    return (<div>
    <Anchor id="about" />
<div className="jumbotron my-0 bg-dark text-white">
  <center>
    <h1 className="d-inline font-weight-bold border-bottom-title border-primary">About</h1>
    <hr className="mt-5 my-4" />
  </center>
  <div className="row pb-2">
    <div className="py-3 col-sm-4 offset-sm-2">MineID is a free OAuth 2.0 authentication service for Minecraft created as a collaboration 
of sites in order to provide a neutral third party solution allowing Minecraft players to login to any 
site using their Minecraft credentials.</div>
    <div className="py-3 col-sm-4">This open-source project was created as a service to the Minecraft community so developers and
site administrators had a simple way to offer a "Login with Minecraft" option to their
users. And for users, a trusted and consistent means to authenticate knowing that their
Minecraft credentials remain secure.</div>
  </div>
  <center>
        <LinkButton type="primary" className="btn-lg font-weight-bold mt-5" href="/#">Connect Your Application</LinkButton>
  </center>
</div>

    </div>);
  }
}

class HowItWorks extends React.Component {
  render() {
    return (<div>
    <Anchor id="how-it-works" />
<div className="jumbotron my-0">
  <center>
    <Title>How it works</Title>
    <hr className="mt-5 my-4" />
  </center>
  <div className="row pb-2">
    <div className="py-3 col-sm-8 offset-sm-2">
      <p>MineID is an OAuth 2.0 provider that provides functionality similar to other OAuth services such as Facebook's "Login with Facebook" button that is common on many sites.</p>
      <p>While Mojang already provides an authentication service for Minecraft, its service is not OAuth compliant. MineID solves this by providing an OAuth wrapper for Mojang's auth service.</p>
      <p className="mb-4">With MineID you can easily add a "Login with Minecraft" button to your site that allows users to create an account and login without your site having to collect and verify the user's credentials.</p>
      <Subtitle>Step 1</Subtitle>
      <p className="my-4">User opts to signup for an website or app using MineID by pressing a "Sign in with Minecraft" button.</p>
      <Subtitle>Step 2</Subtitle>
      <p className="my-4">Pressing the Sign in button redirects the user to MineID. If the user is not already authenticated, MineID prompts the user for their Minecraft username and password. The user's credentials are forwarded to Mojang's server for authentication and discarded.</p>
      <Subtitle>Step 3</Subtitle>
      <p className="my-4">Upon successful authentication, MineID stores only the user's Minecraft player name, id, and email address. This information is returned to the application along with a unique access token that verifies the user's identity.</p>
    </div>
  </div>
</div>
    </div>);
  }
}

class Username extends React.Component {
  render() {
    return utils.getStorage().getItem("username");
  }
}

class Avatar extends React.Component {
  render() {
    return (
      <img className="img-fluid pl-4 pl-md-1 pr-3" src={"https://minotar.net/avatar/" + utils.getStorage().getItem("username") + "/32"} alt= "" />
    );
  }
}

class AuthenticatedVisible extends React.Component {
  render() {
    return utils.isLoggedIn() ? this.props.children : "";
  }
}

class NonAuthenticatedVisible extends React.Component {
  render() {
    return utils.isLoggedIn() ? "" : this.props.children;
  }
}

class Navbar extends React.Component {
  render() {
    return (
<div>
<div className="bg-dark p-3 header border-bottom-title border-warning">
  <nav className="navbar navbar-expand-md navbar-dark container font-weight-bold" id="navbar-main">
    <a className="navbar-brand" href="/#">
      <h3 className="mb-0 font-weight-bold border-bottom-title border-warning">MineID</h3>
    </a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
  
    {<SignInButton className="d-block d-md-none"/>}
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav ml-auto mr-md-5">
        <li className="nav-item px-md-2">
          <a className="nav-link" href="/#about">About</a>
        </li>
        <li className="nav-item px-md-2">
          <a className="nav-link" href="/#how-it-works">How it works</a>
        </li>
        <AuthenticatedVisible>
        <a href="/profile" className="link-no-style">
          <li id="headerProfile" className="nav-item pl-md-3 pr-md-2 border-left border-secondary nav-link text-white">
              <Avatar />
              <Username />
          </li>
        </a>
        </AuthenticatedVisible>
      </ul>
    </div>
    {<SignInButton className="d-none d-md-block"/>}
  </nav>
</div>
<div className="my-4 py-4"></div>
</div>
    );
  }
}

class Footer extends React.Component {
  renderFooterTitle(type, text) {
    return (
      <Subtitle type={type} className="text-white d-block mb-3">{text}</Subtitle>
    );
  }

  render() {
    return (
<footer className="bg-dark text-secondary">
<div className="container">
  <div className="row p-5">
    <div className="col-sm-3">
        {this.renderFooterTitle("danger", "Legal")}
	<span className="font-weight-bold">MineID.net</span> is not affiliated with Mojang AB. "Minecraft" is a trademark of Mojang AB © 2018
    </div>
    <div className="col-sm-3">
        {this.renderFooterTitle("warning", "Links")}
    </div>
    <div className="col-sm-3">
        {this.renderFooterTitle("success", "About")}
	<span className="font-weight-bold">MineID.net</span> is a fully open source software. The code can be found in the links to the left. Merge requests are gladly appreciated.
    </div>
    <div className="col-sm-3">
        {this.renderFooterTitle("primary", "Contact")}
	This website/service was created and is operated by <a href="https://foorack.com/" className="text-primary font-weight-bold">Foorack</a>.
        If you want to contact me please direct message me on <a href="https://twitter.com/Foorack" className="text-primary font-weight-bold">Twitter</a>.
    </div>
  </div>
</div>
</footer>
    );
  }
}

class Alert extends React.Component {
  render() {
    return (
      <div className={"alert alert-" + (this.props.type || "") + " " + (this.props.className || "")} role="alert">
        {this.props.children}
      </div>
    );
  }
}

class Logout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: 0
    }
  }

  logout() {
    localStorage.removeItem("remember");
    localStorage.removeItem("uuid");
    localStorage.removeItem("username");
    sessionStorage.removeItem("uuid");
    sessionStorage.removeItem("username");
    setTimeout(utils.redirect, 3000);
  }

  render() {
    return (
<div className="jumbotron my-0">
  <center>
    <Title>Logout</Title>
    <hr className="my-5" />
  </center>
  <div className="row pb-2">
    <div className="py-3 col-sm-6 offset-sm-3 border-title border-warning bg-light p-3">
      <AuthenticatedVisible>
      <Alert type="success">
        <strong>Logging out...</strong> You are now being redirected...
        {this.logout()}
      </Alert>
      </AuthenticatedVisible>
      <NonAuthenticatedVisible>
      <Alert type="danger">
        <strong>You are not logged in!</strong> Please login before you can logout.
      </Alert>
      </NonAuthenticatedVisible>
    </div>
  </div>
</div>
    );
  }
}

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: 0
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    var remember = document.getElementById("inputRemember").checked;
    utils.send("internal/authenticate", {
      type: 'credentials',
      username: document.getElementById("inputEmail").value,
      password: document.getElementById("inputPassword").value,
      remember: remember
    }, res => {
      console.log(res);
      if(res.status === 200){
        this.setState({alert: 1});
        if(remember === true ){
          localStorage.setItem("remember", remember); // remember should always be in localStorage
        }
        utils.getStorage().setItem("uuid", res.uuid);
        utils.getStorage().setItem("username", res.username);
        setTimeout(utils.redirect, 3000);
      } else {
        this.setState({alert: 2});
      }
    });
  }

  render() {
    return (
<div className="jumbotron my-0">
  <center>
    <Title>Authenticate</Title>
    <hr className="my-5" />
  </center>
  <div className="row pb-2">
    <div className="py-3 col-sm-6 offset-sm-3 border-title border-warning bg-light p-3">
      <Alert type="success" className={(this.state.alert === 1 ? "" : "d-none")}>
        <strong>Authentication successful!</strong> You will now be redirected...
      </Alert>
      <Alert type="danger" className={(this.state.alert === 2 ? "" : "d-none")}>
        <strong>Invalid credentials!</strong> Please check your email and password.
      </Alert>
      <form id="loginCredentialsForm" className="font-weight-bold" onSubmit={this.handleSubmit}>
        <div className="form-group row">
          <label htmlFor="inputEmail" className="col-sm-2 col-form-label">Email</label>
          <div className="col-sm-10">
            <input type="email" className="form-control border-bottom-title" id="inputEmail" placeholder="Enter email" />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="inputPassword" className="col-sm-2 col-form-label">Password</label>
          <div className="col-sm-10">
            <input type="password" className="form-control border-bottom-title" id="inputPassword" placeholder="Password" />
          </div>
        </div>
        <div className="form-check float-right pr-3">
          <input type="checkbox" className="form-check-input" id="inputRemember" />
          <label className="form-check-label" htmlFor="inputRemember">Remember me</label>
        </div>
        <br />
        <br />
        <Button type="success" className="btn-block font-weight-bold">Sign In</Button>
      </form>
    </div>
  </div>
</div>
    );
  }
}

class Profile extends React.Component {
  render() {
    return (
      <div>
      </div>
    );
  }
}

class Home extends React.Component {
  render() {
    return (
      <div>
      <MainIntro />
      <About />
      <HowItWorks />
      </div>
    );
  }
}

class App extends React.Component {
  render() {
    return (
      <div>
        <Navbar />
        <Router>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/logout' component={Logout} />
            <Route exact path='/profile' component={Profile} />
           {/*<Route exact path='/oauth/authorize' component={Authorize} />*/}
          </Switch>
        </Router>
        <Footer />
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
