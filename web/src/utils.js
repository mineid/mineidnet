export function send(path, data, callback) {
  fetch('http://localhost:4567/api/v1/' + path, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: encodeFormData(data)
  }).then((res) => res.json())
  .then(callback)
  .catch(function(error) {
    console.error(error);
  });
}

export function getQuery(param) {
  let params = (new URL(window.location)).searchParams;
  return params.get(param);
}

export function redirect() {
  var redirect_uri = getQuery("redirect_uri");
  if(redirect_uri === null){
    window.location.href = "/";
  } else {
    window.location.href = redirect_uri;
  }
}

export function encodeFormData(data) {
  return Object.keys(data).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
  }).join('&');
}

export function getStorage() {
  if(localStorage.getItem("remember") === "true") {
    return localStorage;
  } else {
    return sessionStorage;
  }
}

export function isLoggedIn() {
  if( getStorage().getItem("username")) {
    return true;
  } else {
    return false;
  }
}
